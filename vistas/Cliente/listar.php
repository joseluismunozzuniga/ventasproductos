<?php

require_once 'vistas/template.php'
?>

<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Listado de Clientes</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v2</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Listado de Clientes</h3>
                        </div>
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>N</th>
                                        <th>Nombres</th>
                                        <th>Apellidos</th>
                                        <th>Telefono</th>
                                        <th>Accion</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    foreach($arrClientes as $cliente) {
                                    ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $cliente->nombres ?></td>
                                            <td><?php echo $cliente->apellidos ?></td>
                                            <td><?php echo $cliente->telefono ?></td>         
                                            <td>
                                                <a class="btn btn-xs btn-warning" href="index.php?c=cliente&cliente=<?php echo $cliente->id ?>" role="button">Editar</a>
                                                <a class="btn btn-xs btn-danger" href="index.php?c=cliente&a=eliminar&id=<?php echo $cliente->id ?>" role="button">Eliminar</a>
                                            </td>
                                        </tr>
                                    <?php $i++;}?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Cierra el content-wrapper-->
</div>
<!-- Cierra el wrapper-->
</div>

<?php require_once 'vistas/footer.php'; ?>

<!-- Cierra el body y el html-->
</body>
</html>