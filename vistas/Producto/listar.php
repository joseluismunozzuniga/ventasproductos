<?php

require_once 'vistas/template.php'
?>

<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Listado de Productos</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v2</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Listado de Productos</h3>
                        </div>
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>N</th>
                                        <th>Nombre</th>
                                        <th>Precio</th>
                                        <th>Accion</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    foreach($arrProductos as $producto) {
                                    ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $producto->nombre ?></td>
                                            <td><?php echo $producto->precio ?></td>
         
                                            <td>
                                                <a class="btn btn-xs btn-warning" href="index.php?c=producto&producto=<?php echo $producto->id ?>" role="button">Editar</a>
                                                <a class="btn btn-xs btn-danger" href="index.php?c=producto&a=eliminar&id=<?php echo $producto->id ?>" role="button">Eliminar</a>
                                            </td>
                                        </tr>
                                    <?php $i++;}?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Cierra el content-wrapper-->
</div>
<!-- Cierra el wrapper-->
</div>

<?php require_once 'vistas/footer.php'; ?>

<!-- Cierra el body y el html-->
</body>
</html>