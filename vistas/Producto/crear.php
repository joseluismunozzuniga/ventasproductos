<?php
    require_once 'vistas/template.php';
?>

<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Agregar Productos</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v2</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Nuevo Producto</h3>
                        </div>
                        <form class="form-horizontal" action="index.php?c=producto&a=guardar" method="post">

                            <div class="card-body">

                                <div class="form-group row">
                                    <label for="producto_nombre" class="col-sm-2 control-label">Nombre</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="producto_nombre" name="producto_nombre" placeholder="Ingrese el nombre del producto">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="producto_precio" class="col-sm-2 control-label">Precio</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="producto_precio" name="producto_precio" placeholder="Ingrese precio del producto">
                                    </div>
                                </div>
                            
                                </div>
                               
                            </div>
                           
                            <?php
                            if(isset($_GET['producto'])){

                                $id_Producto=$_GET['producto'];
                            }else{
                                $id_Producto=null;
                            }
                            
                            ?>
                             <input class="form-control" name="producto_id" id="producto_id" value=<?php echo $id_Producto?>>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-info">Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
<!-- Cierra el content-wrapper-->
</div>
<!-- Cierra el wrapper-->
</div>

<?php require_once 'vistas/footer.php'; ?>

<!-- Cierra el body y el html-->
</body>
</html>