<?php
$controller = 'producto';

//Si no se ha pasada un controlador, se accede al controlador por defecto
if(!isset($_REQUEST['c'])) {
    $nombreControlador = ucwords($controller) . 'Controller';
    require_once "controlador/" . $nombreControlador . ".php";
    $controller = new $nombreControlador();
    $controller->index();    
}else {
    //Se carga el controlador ingresado por url
    $controller = strtolower($_REQUEST['c']);
    $accion = isset($_REQUEST['a']) ? $_REQUEST['a'] : 'index';
    
    //Se instancia el controlador
    $nombreControlador = ucwords($controller) . 'Controller';
    require_once "controlador/" . $nombreControlador . ".php";
    $controller = new $nombreControlador();
    
    // Llama la accion
    call_user_func( array( $controller, $accion ) );
}