<?php
class Conexion {

    private $connection;
    private $host = 'localhost';
    private $db = 'db_pedidos';
    private $user = 'root';
    private $pass = '';

    public function __construct() {
        try {
            $this->connection = new PDO("mysql:host=$this->host;dbname=$this->db"
                    . ";charset=utf8", $this->user, $this->pass);

            // Para que genere excepciones a la hora de reportar errores.
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function query($sql) {
        if ($sql != '') {
            return $this->connection->query($sql);
        } else {
            return 0;
        }
    }

}

