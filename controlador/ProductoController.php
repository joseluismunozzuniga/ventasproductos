<?php
require_once("dao/ProductoDao.php");

class ProductoController{
    public function index(){

        require_once 'vistas/Producto/crear.php';

    }
    public function guardar(){
        $ProductoDao=new ProductoDao();
        $newProducto= new Producto();
        $newProducto->setNombre($_POST['producto_nombre']);
        $newProducto->setPrecio($_POST['producto_precio']);
        $newProducto->setIdProducto(1);
    
        $id=$_POST['producto_id'];
        if($id){
            //Se actualiza en la BD
            $ProductoDao->updateProducto($id,$newProducto);
        }else{
            //Se guardar en la BD
            $ProductoDao->insertProducto($newProducto);
        }
         header('Location: index.php');

    }
    public function listarProductos(){
        $productoDao = new ProductoDao();

        $arrProductos = $productoDao->selectAllProductos();
  
        require_once 'vistas/Producto/listar.php';
    }
    public function eliminar(){
        $productoDao=new ProductoDao();

        $productoDao->deleteProducto($_GET['id']);
        header('Location: index.php?c=producto&a=listarProductos');

        

    }
    







}